package ru.mbakanov.tm;

import ru.mbakanov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("== Welcome to TASK MANAGER ==");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg != null) {
            switch (arg) {
                case TerminalConst.HELP:
                    showHelp();
                    break;
                case TerminalConst.ABOUT:
                    showAbout();
                    break;
                case TerminalConst.VERSION:
                    showVersion();
                    break;
                case TerminalConst.EXIT:
                    exit();
                    break;
                default:
                    showUnexpected(arg);
                    break;
            }
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args.length != 0 & args != null) {
            final String arg = args[0];
            parseArg(arg);
            return true;
        } else return false;
    }

    private static void showHelp() {
        System.out.println("[" + TerminalConst.HELP + "]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show application version.");
        System.out.println(TerminalConst.HELP + " - Show terminal`s commands.");
        System.out.println(TerminalConst.EXIT + " - Close application.");
    }

    private static void showVersion() {
        System.out.println("[" + TerminalConst.VERSION + "]");
        System.out.println(TerminalConst.VERSION_NUM);
    }

    private static void showAbout() {
        System.out.println("[" + TerminalConst.ABOUT + "]");
        System.out.println(TerminalConst.DEV_NAME);
        System.out.println(TerminalConst.DEV_EMAIL);
    }

    private static void showUnexpected(final String arg) {
        System.out.println("['" + arg + "' - unexpected command]");
    }

    private static void exit() {
        System.exit(0);
    }
}
